$(document).ready(function() {
	$('input[type="checkbox"][data-id="playlist-checkbox"]').change(function() {
		let playlistId = $(this).data('value');
		let backupDates = $('[data-id="playlist-backupDates"][data-value="' + playlistId + '"');
		let backupTime = $('[data-id="playlist-backupTime"][data-value="' + playlistId + '"');
		let backupNameScheme = $('[data-id="playlist-backupNameScheme"][data-value="' + playlistId + '"');
		if ($(this).prop('checked')) {
			backupDates.css("display", "block");
			backupTime.css("visibility", "visible");
			backupTime.find('input').attr('required', 'required');
			backupNameScheme.css("visibility", "visible");
			backupNameScheme.find('input').attr('required', 'required');
		} else {
			backupDates.css("display", "none");
			backupTime.css("visibility", "hidden");
			backupTime.find('input').removeAttr('required');
			backupNameScheme.css("visibility", "hidden");
			backupNameScheme.find('input').removeAttr('required');
		}
	});
});