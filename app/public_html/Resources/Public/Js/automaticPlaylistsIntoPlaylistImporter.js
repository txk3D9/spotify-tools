$(document).ready(function() {
	$('input[type="checkbox"][data-id="playlist-checkbox"]').change(function() {
		let playlistId = $(this).data('value');
		let importDates = $('[data-id="playlist-importDates"][data-value="' + playlistId + '"');
		let importTime = $('[data-id="playlist-importTime"][data-value="' + playlistId + '"');
		if ($(this).prop('checked')) {
			importDates.css("visibility", "visible");
			importTime.css("visibility", "visible");
			importTime.find('input').attr('required', 'required');
		} else {
			importDates.css("visibility", "hidden");
			importTime.css("visibility", "hidden");
			importTime.find('input').removeAttr('required');
		}
	});
});