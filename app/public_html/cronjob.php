<?php
ini_set('display_errors', 1);
error_reporting(E_ERROR | E_WARNING);
session_start();
require_once dirname(__FILE__).'/../vendor/autoload.php';

try {
	\SpotifyTools\Utilities\DatabaseUtility::connect();
	$name = isset($argv[1]) ? $argv[1] : $_GET['name'];
	if (!$name) {
		throw new \Exception('No cronjob set');
	}
	$cronjobHandler = new \SpotifyTools\Handlers\CronjobHandler();
	$cronjobHandler->handleCronjob($name, $argv);
} catch (\Exception $e) {
	throw $e;
} finally {
	\SpotifyTools\Utilities\DatabaseUtility::closeConnection();
}

