<?php

namespace SpotifyTools\Handlers;

use SpotifyTools\Actions\AbstractAction;
use SpotifyTools\Actions\ArtistTracksFromPlaylistToPlaylist\SelectPlaylist;
use SpotifyTools\Actions\ArtistTracksFromPlaylistToPlaylist\Submit;
use SpotifyTools\Actions\Overview;
use SpotifyTools\Cronjobs\AbstractCronjob;
use SpotifyTools\Services\SpotifyWebApi;

class CronjobHandler {

	public function handleCronjob($name, $args) {
		if (!array_key_exists($name, AbstractCronjob::CRONJOB_NAME_TO_CLASS)) {
			throw new \Exception('Cronjob "'.$name.'" not found');
		}
		$cronjobClass = AbstractCronjob::CRONJOB_NAME_TO_CLASS[$name];
		/**
		 * @var AbstractCronjob $cronjob
		 */
		$cronjob = new $cronjobClass();
		$cronjob->execute($args);
	}
}