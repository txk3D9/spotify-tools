<?php

namespace SpotifyTools\Handlers;

use SpotifyTools\Actions\AbstractAction;
use SpotifyTools\Actions\ArtistTracksFromPlaylistToPlaylist\SelectPlaylist;
use SpotifyTools\Actions\ArtistTracksFromPlaylistToPlaylist\Submit;
use SpotifyTools\Actions\Overview;
use SpotifyTools\Services\SpotifyWebApi;

class RequestHandler {


	public function handleRequest(SpotifyWebApi $api) {
		if(array_key_exists('action', $_GET) && $_GET['action']) {
			$action = $_GET['action'];
		} else {
			$action = Overview::ACTION_NAME;
		}
		if(!array_key_exists($action, AbstractAction::ACTION_NAME_TO_CLASS)) {
			throw new \Exception('Action "'.$action.'" not found');
		}
		$actionClass = AbstractAction::ACTION_NAME_TO_CLASS[$action];
		/**
		 * @var AbstractAction $action
		 */
		$action = new $actionClass($api);
		$action->execute();
	}
}