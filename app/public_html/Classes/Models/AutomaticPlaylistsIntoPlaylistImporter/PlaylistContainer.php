<?php

namespace SpotifyTools\Models\AutomaticPlaylistsIntoPlaylistImporter;

class PlaylistContainer  {

	private $spotifyPlaylist;
	private $databaseImportPlaylist;

	/**
	 * @return mixed
	 */
	public function getSpotifyPlaylist() {
		return $this->spotifyPlaylist;
	}

	/**
	 * @param mixed $spotifyPlaylist
	 */
	public function setSpotifyPlaylist($spotifyPlaylist): void {
		$this->spotifyPlaylist = $spotifyPlaylist;
	}

	/**
	 * @return mixed
	 */
	public function getDatabaseImportPlaylist() {
		return $this->databaseImportPlaylist;
	}

	/**
	 * @param mixed $databaseImportPlaylist
	 */
	public function setDatabaseImportPlaylist($databaseImportPlaylist): void {
		$this->databaseImportPlaylist = $databaseImportPlaylist;
	}
}