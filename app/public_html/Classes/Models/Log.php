<?php

namespace SpotifyTools\Models;

use SpotifyTools\Utilities\LogUtility;

class Log {

	const INFO = 'info';
	const WARNING = 'warning';
	const ERROR = 'error';
	private $requestId;
	private $logLevel;
	private $message;
	private $data;
	private $userId;

	/**
	 * @param $logLevel
	 * @param $message
	 * @param $data
	 */
	public function __construct($logLevel = null, $message = null, $data = null) {
		$this->logLevel = $logLevel;
		$this->message = $message;
		$this->data = $data;
		$this->requestId = LogUtility::getUniqueLogIdForCurrentRequest();
	}

	/**
	 * @return mixed
	 */
	public function getRequestId() {
		return $this->requestId;
	}

	/**
	 * @param mixed $requestId
	 */
	public function setRequestId($requestId): void {
		$this->requestId = $requestId;
	}

	/**
	 * @return mixed
	 */
	public function getLogLevel() {
		return $this->logLevel;
	}

	/**
	 * @return mixed
	 */
	public function getMessage() {
		return $this->message;
	}

	/**
	 * @return mixed
	 */
	public function getData() {
		return $this->data;
	}

	/**
	 * @return mixed
	 */
	public function getUserId() {
		return $this->userId;
	}

	/**
	 * @param mixed $userId
	 */
	public function setUserId($userId): void {
		$this->userId = $userId;
	}


}