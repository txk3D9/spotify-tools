<?php

namespace SpotifyTools\Models\AutomaticPlaylistBackup;

class PlaylistContainer  {

	private $spotifyPlaylist;
	private $databasePlaylist;

	/**
	 * @return mixed
	 */
	public function getSpotifyPlaylist() {
		return $this->spotifyPlaylist;
	}

	/**
	 * @param mixed $spotifyPlaylist
	 */
	public function setSpotifyPlaylist($spotifyPlaylist): void {
		$this->spotifyPlaylist = $spotifyPlaylist;
	}

	/**
	 * @return mixed
	 */
	public function getDatabasePlaylist() {
		return $this->databasePlaylist;
	}

	/**
	 * @param mixed $databasePlaylist
	 */
	public function setDatabasePlaylist($databasePlaylist): void {
		$this->databasePlaylist = $databasePlaylist;
	}
}