<?php

namespace SpotifyTools\Models;

class Config
{

    protected $clientId;
	protected $clientSecret;
	protected $baseUrl;
	protected $database;

	/**
	 * @return mixed
	 */
	public function getDatabase() {
		return $this->database;
	}

	/**
	 * @param mixed $database
	 */
	public function setDatabase($database) {
		$this->database = $database;
	}

    /**
     * @return mixed
     */
    public function getBaseUrl()
    {
        return $this->baseUrl;
    }

    /**
     * @param mixed $baseUrl
     */
    public function setBaseUrl($baseUrl)
    {
        $this->baseUrl = $baseUrl;
    }

    /**
     * @return mixed
     */
    public function getClientId()
    {
        return $this->clientId;
    }

    /**
     * @param mixed $clientId
     */
    public function setClientId($clientId)
    {
        $this->clientId = $clientId;
    }

    /**
     * @return mixed
     */
    public function getClientSecret()
    {
        return $this->clientSecret;
    }

    /**
     * @param mixed $clientSecret
     */
    public function setClientSecret($clientSecret)
    {
        $this->clientSecret = $clientSecret;
    }
}