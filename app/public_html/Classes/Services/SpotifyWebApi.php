<?php

namespace SpotifyTools\Services;

use SpotifyTools\Models\Log;
use SpotifyTools\Utilities\CurlUtility;
use SpotifyTools\Utilities\LogUtility;
use SpotifyTools\Utilities\UserUtility;

class SpotifyWebApi extends \SpotifyWebAPI\SpotifyWebAPI {

    public function getAllMySavedTracks($options = ['fields' => 'items(name,id),next']) {
        return $this->loopForAll('/v1/me/tracks', 50, $options);
    }

    public function getAllRecentTracks($options = ['fields' => 'track(name,id),played_at,next,cursors']) {
        return $this->loopForAllForRecentTracks('/v1/me/player/recently-played', 50, $options);
    }

	public function getAllMyPlaylists($options = ['fields' => ['id,description,name']]) {
		return $this->loopForAll('/v1/me/playlists', 50, $options);
	}

	public function getAllPlaylistTracks($playlistId, $options = []) {
		LogUtility::log(new Log(Log::INFO, "getAllPlaylistTracks start", ["playlistId" => $playlistId, "options" => $options]));
		$result = $this->loopForAll('/v1/playlists/'.$playlistId.'/tracks', 100, $options);
		LogUtility::log(new Log(Log::INFO, "getAllPlaylistTracks end"));
		return $result;
	}

	public function getAllArtistAlbums($artistId, $options = []) {
		return $this->loopForAll('/v1/artists/'.$artistId.'/albums', 50, $options);
	}

	public function getAllAlbumTracks($albumId, $options = ['fields' => 'items(name,id,artists),next']) {
		return $this->loopForAll('/v1/albums/'.$albumId.'/tracks', 50, $options);
	}

	public function getTopArtists($timeRange, $maxResults = 150) {
		return $this->loopForTops('/v1/me/top/artists', $maxResults, ['time_range' => $timeRange]);
	}

	public function getTopTracks($timeRange, $maxResults = 150) {
		return $this->loopForTops('/v1/me/top/tracks', $maxResults, ['time_range' => $timeRange]);
	}

	public function addAllPlaylistTracks($playlistId, array $trackIds) {
		try {
			LogUtility::log(new Log(Log::INFO, "addAllPlaylistTracks start", ["playlistId" => $playlistId, "trackIds" => $trackIds]));
			foreach (array_chunk($trackIds, 50) as $chunkedTracks) {
				LogUtility::log(new Log(Log::INFO, "addAllPlaylistTracks chunk start", ["playlistId" => $playlistId, "trackIds" => $chunkedTracks]));
				$this->addPlaylistTracks($playlistId, $chunkedTracks);
				LogUtility::log(new Log(Log::INFO, "addAllPlaylistTracks chunk response", ["lastResponse" => $this->lastResponse]));
				sleep(1);
			}
			LogUtility::log(new Log(Log::INFO, "addAllPlaylistTracks done"));
		} catch (\Exception $e) {
			LogUtility::log(new Log(Log::INFO, "addAllPlaylistTracks error", ["lastResponse" => $this->lastResponse]));
			throw $e;
		}
	}

	public function createPlaylist($options) {
		LogUtility::log(new Log(Log::INFO, "createPlaylist", $options));
		$playlist =  parent::createPlaylist($options);
		LogUtility::log(new Log(Log::INFO, "createdPlaylist", $playlist));
		return $playlist;
	}

	public function deleteAllMyTracks(array $trackIds) {
		foreach (array_chunk($trackIds, 50) as $chunkedTracks) {
			$this->deleteMyTracks($chunkedTracks);
		}
	}

	public function deleteAllPlaylistTracks($playlistId, array $trackIds) {
		foreach (array_chunk($trackIds, 100) as $chunkedTracks) {
			$tracksToDeleteArray = ["tracks" => []];
			foreach ($chunkedTracks as $trackId) {
				$tracksToDeleteArray["tracks"][] = ["uri" => $trackId];
			}
			$this->deletePlaylistTracks($playlistId, $tracksToDeleteArray);
		}
	}

	public function deleteAllPlaylistTracksWithPositions($playlistId, array $tracksWithPositions, $playlistSnapshotId) {
		foreach (array_chunk($tracksWithPositions, 100) as $chunkedTracksWithPosition) {
			$tracksToDeleteArray = ["tracks" => []];
			foreach ($chunkedTracksWithPosition as $item) {
				$tracksToDeleteArray["tracks"][] = ["uri" => $item["uri"], "positions" => $item["positions"]];
			}
			$this->deletePlaylistTracks($playlistId, $tracksToDeleteArray, $playlistSnapshotId);
		}
	}

	public function allTracksSearch($query, $options = []) {
		$options = array_merge((array) $options, [
			'q' => $query,
			'type' => 'track',
		]);
		$options['limit'] = 50;
		$options['offset'] = 0;
		$uri = '/v1/search';
		$body = null;
		do {
			$this->lastResponse = $this->sendRequest('GET', $uri, $options);
			if ($body == null) {
				$body = $this->lastResponse['body'];
			} else {
				$body->tracks->items = array_merge($body->tracks->items, $this->lastResponse['body']->tracks->items);
			}
			$options['offset'] = $options['offset'] + $options['limit'];
		} while ($this->lastResponse['status'] == 200 && isset($this->lastResponse['body']->tracks->next));

		return $body;
	}

	private function loopForAllForRecentTracks($uri, $limitPerRequest = 50, $options = [], $maxResults = null) {
		$options['limit'] = $limitPerRequest;
		$options['offset'] = 0;
		$body = null;
		do {
            // dafuq, pls refactor this shit
            if($options['offset'] > 50) {
                return $body;
            }

			$this->lastResponse = $this->sendRequest('GET', $uri, $options);
			if ($body == null) {
				$body = $this->lastResponse['body'];
			} else {
				$body->items = array_merge($body->items, $this->lastResponse['body']->items);
			}
			$options['offset'] = $options['offset'] + $options['limit'];
		} while ($this->lastResponse['status'] == 200
		&& isset($this->lastResponse['body']->next)
		&& (!$maxResults || ($options['offset'] + $limitPerRequest) < $maxResults));

		return $body;
	}

    private function loopForAll($uri, $limitPerRequest = 50, $options = [], $maxResults = null) {
        $options['limit'] = $limitPerRequest;
        $options['offset'] = 0;
        $body = null;
        do {
            $this->lastResponse = $this->sendRequest('GET', $uri, $options);
            if ($body == null) {
                $body = $this->lastResponse['body'];
            } else {
                $body->items = array_merge($body->items, $this->lastResponse['body']->items);
            }
            $options['offset'] = $options['offset'] + $options['limit'];
        } while ($this->lastResponse['status'] == 200
        && isset($this->lastResponse['body']->next)
        && (!$maxResults || ($options['offset'] + $limitPerRequest) < $maxResults));

        return $body;
    }

	//the api has a bug. if you set limit and offset to 50 no response will come. if you set offset to 49, it will return 50 items.
	//so we can only get the top 99 tracks/artists...
	private function loopForTops($uri, $maxResults, $options = []) {
		$limitPerRequest = 50;
		$options['limit'] = $limitPerRequest;
		$options['offset'] = 0;
		$body = null;
		do {
			$this->lastResponse = $this->sendRequest('GET', $uri, $options);
			if ($body == null) {
				$body = $this->lastResponse['body'];
			} else {
				$body->items = array_merge($body->items, $this->lastResponse['body']->items);
			}
			$options['offset'] = $options['offset'] + 49;
		} while ($this->lastResponse['status'] == 200
			&& $this->lastResponse['body']->items
			&& count($this->lastResponse['body']->items) > 0
			&& (!$maxResults || ($options['offset'] + $limitPerRequest) < $maxResults));

		unset($body->items[50]); //remove duplicates....

		return $body;
	}
}