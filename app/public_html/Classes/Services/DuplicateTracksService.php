<?php

namespace SpotifyTools\Services;

class DuplicateTracksService {

	const SELECT_FIELDS = 'items(track(id,name,uri,artists(name,id))),next';

	public function removeDuplicates($items) {
		$groupedByName = $this->groupByName($items);
		$groupedByDuplicate = $this->groupByDuplicate($groupedByName);

		return $this->getUniqueTrackFromGroups($groupedByDuplicate);
	}

	//returns only duplicates, but only one of each duplicate group
	public function getUniqueDuplicates($items) {
		$groupedByName = $this->groupByName($items);
		$this->removeGroupsWithOnlyOneItem($groupedByName);
		$groupedByDuplicate = $this->groupByDuplicate($groupedByName);
		$this->removeGroupsWithOnlyOneItem($groupedByDuplicate);

		return $this->getUniqueTrackFromGroups($groupedByDuplicate);
	}

	public function packDuplicatesTogether($items) {
		$groupedByName = $this->groupByName($items);
		$this->removeGroupsWithOnlyOneItem($groupedByName);
		$groupedByDuplicate = $this->groupByDuplicate($groupedByName);
		$this->removeGroupsWithOnlyOneItem($groupedByDuplicate);

		return $groupedByDuplicate;
	}

	private function getUniqueTrackFromGroups($groups) {
		$uniqueTracks = [];
		foreach ($groups as $key => $pack) {
			$uniqueTracks[$key] = current($pack);
		}

		return $uniqueTracks;
	}

	private function groupByName($items) {
		//alter the track names for easier comparison
		$items = array_map(function ($a) {
			$a->track->alteredName = $this->alterTrackName($a->track->name);

			return $a;
		}, $items);
		//group all tracks with the same name together
		$groupedByName = [];
		foreach ($items as $key => $item) {
			$item->track->positionInPlaylist = $key;
			$groupedByName[$item->track->alteredName][] = $item;
		}

		return $groupedByName;
	}

	private function removeGroupsWithOnlyOneItem(&$group) {
		foreach ($group as $key => $items) {
			if (count($items) == 1) {
				unset($group[$key]);
			}
		}
	}

	private function groupByDuplicate($groupedByName) {
		$groupedByDuplicate = [];
		//group by duplicates
		foreach ($groupedByName as $items) {
			$usedKeys = [];
			foreach ($items as $firstKey => $firstItem) {
				if (!in_array($firstKey, $usedKeys)) {
					$groupedByDuplicate[$firstItem->track->id][] = $firstItem;
					foreach ($items as $secondKey => $secondItem) {
						if ($firstKey == $secondKey) {
							continue; //skip if its the same entry or already for deletion marked
						} else if ($this->areTracksDuplicate($firstItem, $secondItem)) {
							$groupedByDuplicate[$firstItem->track->id][] = $secondItem;
							unset($items[$secondKey]);
							$usedKeys[] = $secondKey;
						}
					}
					unset($items[$firstKey]);
				}
			}
		}

		return $groupedByDuplicate;
	}

	private function areTracksDuplicate($track1, $track2) {
		if ($track1->track->id == $track2->track->id) {
			return true;
		}
		$artistNamesTrack1 = array_map(function ($artist) {
			return trim(strtolower($artist->name));
		}, $track1->track->artists);
		$artistNamesTrack2 = array_map(function ($artist) {
			return trim(strtolower($artist->name));
		}, $track2->track->artists);
		sort($artistNamesTrack1);
		sort($artistNamesTrack2);
		if ($artistNamesTrack1 == $artistNamesTrack2) {
			return true;
		}

		return false;
	}

	private function alterTrackName($string) {
		$newString =
			trim(
				preg_replace('/\s+/', ' ',  //remove double whitespaces
					preg_replace('/[^A-Za-z0-9 ]/', '', $string) //remove special chars
				)
			);
		if ($newString) { //some tracks have special chars (fe russian songs) and will have an emptry string than
			$string = $newString;
		}
		$wordsToCutOffFrom = [' feat ', ' Feat ', ' Original Mix'];
		foreach ($wordsToCutOffFrom as $wordToCutOffFrom) {
			if (strpos($string, $wordToCutOffFrom) !== false) {
				$string = substr($string, 0, strpos($string, $wordToCutOffFrom));
			}
		}

		return trim($string);
	}
}