<?php

namespace SpotifyTools\Services;

use SpotifyTools\Exceptions\AccessTokenRefreshNotSuccessfulException;
use SpotifyTools\Exceptions\NoAccessTokenAvailableException;
use SpotifyTools\Models\Config;
use SpotifyTools\Utilities\ConfigUtility;
use SpotifyTools\Utilities\DatabaseUtility;
use SpotifyTools\Utilities\UserUtility;
use SpotifyWebAPI\Session;
use SpotifyWebAPI\SpotifyWebAPIAuthException;

class AuthService {

	public function handleWebAuthentication() {
		$config = ConfigUtility::getConfig();
		$api = new SpotifyWebApi();
		try {
			$dbUser = UserUtility::getUserFromPHPSession();
			if ($dbUser) {
				if ($dbUser['token_expiration'] <= time() + 60) { //1 minute extra so the access token doesnt expire in runtime
					$session = $this->createSessionFromConfig($config);
					$refreshSuccessful = $session->refreshAccessToken($dbUser['refresh_token']);
					if (!$refreshSuccessful) {
						throw new AccessTokenRefreshNotSuccessfulException();
					}
					$api->setAccessToken($session->getAccessToken());
					$this->saveUserFromSpotifySessionToDatabase($session, $api);
				} else {
					$api->setAccessToken($dbUser['access_token']);
				}

				return $api;
			} else {
				throw new NoAccessTokenAvailableException();
			}
		} catch (NoAccessTokenAvailableException | AccessTokenRefreshNotSuccessfulException $e) {
			$session = $this->createSessionFromConfig($config);
			if (isset($_GET['code'])) {
				$session->requestAccessToken($_GET['code']);
				$api->setAccessToken($session->getAccessToken());
				AuthService::saveUserFromSpotifySessionToDatabase($session, $api);
				header('Location: '.$config->getBaseUrl());
				exit();
			} else {
				$options = [
					'scope' => [
						//Images
						'ugc-image-upload',
						//Listening History
						'user-read-recently-played',
						'user-top-read',
						'user-read-playback-position',
						//Spotify Connect
						'user-read-playback-state',
						'user-modify-playback-state',
						'user-read-currently-playing',
						//Playback
						'app-remote-control',
						'streaming',
						//Playlists
						'playlist-modify-public',
						'playlist-modify-private',
						'playlist-read-private',
						'playlist-read-collaborative',
						//Follow
						'user-follow-modify',
						'user-follow-read',
						//Library
						'user-library-modify',
						'user-library-read',
						//Users
						'user-read-email',
						'user-read-private',
					],
				];
				header('Location: '.$session->getAuthorizeUrl($options));
				exit();
			}
		} catch (SpotifyWebAPIAuthException $e) {
			header('Location: '.$config->getBaseUrl());
			exit();
		}
	}

	public function getSpotifyWebApiForDbUser($dbUser) {
		$api = new SpotifyWebApi();
		if ($dbUser['token_expiration'] <= time() + 60) { //1 minute extra so the access token doesnt expire in runtime
			$config = ConfigUtility::getConfig();
			$session = $this->createSessionFromConfig($config);
			$refreshSuccessful = $session->refreshAccessToken($dbUser['refresh_token']);
			if (!$refreshSuccessful) {
				throw new AccessTokenRefreshNotSuccessfulException();
			}
			$api->setAccessToken($session->getAccessToken());
			$this->updateUserFromSpotifySession($dbUser, $session);
		} else {
			$api->setAccessToken($dbUser['access_token']);
		}

		return $api;
	}

	private function createSessionFromConfig(Config $config) {
		return new Session(
			$config->getClientId(),
			$config->getClientSecret(),
			$config->getBaseUrl()
		);
	}

	private function saveUserFromSpotifySessionToDatabase(Session $session, SpotifyWebApi $api) {
		$user = UserUtility::getSpotifyUser($api);
		$userId = $user->id;
		$result = DatabaseUtility::getConnection()->query("SELECT * FROM users WHERE user_id = ".$userId);
		if ($result && $result->num_rows > 0) {
			DatabaseUtility::getConnection()->query("DELETE FROM users WHERE user_id = ".$userId);
		}
		DatabaseUtility::executeBindedQuery("INSERT INTO users 
			(user_id, display_name, php_session_id, access_token, refresh_token, token_expiration)
			VALUES (?, ?, ?, ?, ?, ?)",
		"sssssi",
		[$userId, $user->display_name, session_id(), $session->getAccessToken(), $session->getRefreshToken(), $session->getTokenExpiration()]);
	}

	private function updateUserFromSpotifySession($dbUser, Session $session) {
		DatabaseUtility::executeBindedQuery("UPDATE users 
			SET access_token = ?,
			 	refresh_token = ?,
			  	token_expiration = ? 
			WHERE user_id = ?",
			"ssss",
			[$session->getAccessToken(), $session->getRefreshToken(), $session->getTokenExpiration(), $dbUser['user_id']]);
	}
}