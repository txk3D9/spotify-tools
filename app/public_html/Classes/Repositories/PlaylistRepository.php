<?php

namespace SpotifyTools\Repositories;

use SpotifyTools\Models\Log;
use SpotifyTools\Utilities\DatabaseUtility;
use SpotifyTools\Utilities\LogUtility;
use SpotifyWebAPI\SpotifyWebAPI;

class PlaylistRepository
{

    public static function getPlaylist($playlistId, SpotifyWebAPI $api) {
        LogUtility::log(new Log('info',
            'PlaylistRepository getPlaylist start',
            ['playlistId' => $playlistId]));
        $playlist = $api->getPlaylist($playlistId, ['fields' => ['name', 'id', 'snapshot_id']]);

        $result = DatabaseUtility::executeBindedQuery("SELECT * FROM playlists
            WHERE playlistId = ?
            AND snapshotId = ?",
            "ss",
            [$playlistId, $playlist->snapshot_id])
            ->fetch_assoc();
        if($result) {
            LogUtility::log(new Log('info',
                'PlaylistRepository getPlaylist end (found in database)',
                ['playlistId' => $playlistId]));
            return json_decode($result['data']);
        }
        LogUtility::log(new Log('info',
            'PlaylistRepository getPlaylist start gathering tracks from spotify',
            ['playlistId' => $playlistId]));
        $tracks = $api->getAllPlaylistTracks($playlistId, ['fields' => 'items(track(id,name,artists(name,id))),next']);
        $playlist->tracks = $tracks->items;
        LogUtility::log(new Log('info',
            'PlaylistRepository getPlaylist gathering tracks done, saving to database',
            ['playlistId' => $playlistId]));
        DatabaseUtility::executeBindedQuery("INSERT INTO playlists 
		    (playlistId, snapshotId, `data`)
			VALUES (?, ?, ?)",
            "sss",
            [$playlistId, $playlist->snapshot_id, json_encode($playlist)]
        );

        LogUtility::log(new Log('info',
            'PlaylistRepository getPlaylist end (gathered from spotify api)', ['playlistId' => $playlistId]));
        return $playlist;
    }
}