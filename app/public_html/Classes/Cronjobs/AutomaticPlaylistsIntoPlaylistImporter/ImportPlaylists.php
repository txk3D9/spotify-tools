<?php

namespace SpotifyTools\Cronjobs\AutomaticPlaylistsIntoPlaylistImporter;

use SpotifyTools\Cronjobs\AbstractCronjob;
use SpotifyTools\Cronjobs\Traits\PlaylistHandlingTrait;
use SpotifyTools\Services\SpotifyWebApi;
use SpotifyTools\Utilities\AuthService;
use SpotifyTools\Utilities\DatabaseUtility;
use SpotifyTools\Utilities\DebugUtility;
use SpotifyTools\Utilities\UserUtility;

class ImportPlaylists extends AbstractCronjob {

	use PlaylistHandlingTrait;

	const CRONJOB_NAME = 'apipi_importPlaylists';

	function execute($args) {
		$importPlaylists = $this->getDueImportPlaylists();
		if ($importPlaylists) {
			foreach ($importPlaylists as $importPlaylist) {
				$dbUser = UserUtility::getUserById($importPlaylist['user_id']);
				$this->api = $this->authService->getSpotifyWebApiForDbUser($dbUser);
				$this->addImportPlaylistTracksToPlaylist($importPlaylist);
				$this->updateLastImportDate($importPlaylist);
			}
		}
	}

	private function addImportPlaylistTracksToPlaylist($importPlaylist) {
		$importPlaylistId = $importPlaylist['import_playlist_id'];
		$playlistId = $importPlaylist['playlist_id'];
		$importPlaylistTracks = $this->getTracksByPlaylist($importPlaylistId);
		$playlistTracks = $this->getTracksByPlaylist($playlistId);
		// get difference between the 2 playlists
		$intersect = array_intersect_key($playlistTracks, $importPlaylistTracks); //gets all same tracks
		$diff = array_diff_key($importPlaylistTracks, $intersect); //gets difference between to import and already available tracks
		//add different tracks to playlist
		$this->diffCounter += count($diff);
		$this->api->addAllPlaylistTracks($playlistId, array_keys($diff));
		$this->tracksByPlaylist[$playlistId] = array_merge($this->tracksByPlaylist[$playlistId], $diff);
	}

	private function getDueImportPlaylists() {
		$todaysWeekDay = strtolower(date('l'));
		$result = DatabaseUtility::getConnection()->query('SELECT * FROM apipi_import_playlists
			WHERE '.$todaysWeekDay.' = 1
			AND import_time <= "'.date('H:i:s').'"
			AND (
				last_import IS NULL 
					OR
				last_import < CONCAT("'.date('Y-m-d').'", " ", import_time)
				)
			ORDER BY user_id
			');

		return $result->fetch_all(MYSQLI_ASSOC);
	}

	private function updateLastImportDate($importPlaylist) {
		DatabaseUtility::executeBindedQuery("UPDATE apipi_import_playlists 
			SET last_import = '".date('Y-m-d H:i:s')."'
			WHERE playlist_id = ?
			AND user_id = ?
			AND import_playlist_id = ? ",
			"sss",
			[$importPlaylist["playlist_id"], $importPlaylist["user_id"], $importPlaylist["import_playlist_id"]]
		);
	}
}