<?php

namespace SpotifyTools\Cronjobs\AutomaticPlaylistsIntoPlaylistImporter;

use SpotifyTools\Cronjobs\AbstractCronjob;
use SpotifyTools\Cronjobs\Traits\PlaylistHandlingTrait;
use SpotifyTools\Utilities\AuthService;
use SpotifyTools\Utilities\DatabaseUtility;
use SpotifyTools\Utilities\UserUtility;
use SpotifyWebAPI\SpotifyWebAPIException;

class DeleteTracksWhichAreNotInImportPlaylists extends AbstractCronjob {

	use PlaylistHandlingTrait;

	const CRONJOB_NAME = 'apipi_deleteTracksWhichAreNotInImportPlaylists';
	private const MAX_PLAYLIST_RUN = 1;
	private const MIN_DELAY_DAYS_FOR_CHECK = 2;

	function execute($args) {
		$playlists = $this->getPlaylistsForDeletion();
		if($playlists) {
			foreach($playlists as $playlist) {
				try {
					$this->removeTracksNotInImportPlaylists($playlist);
				} catch (SpotifyWebAPIException $e) {
					//exception if api key is not valid anymore
				} finally {
					$this->updateLastDeletionDate($playlist);
				}
			}
		}
	}

	private function removeTracksNotInImportPlaylists($playlist) {
		$importPlaylists = $this->getImportPlaylistsForPlaylist($playlist);
		$dbUser = UserUtility::getUserById($playlist['user_id']);
		$this->api = $this->authService->getSpotifyWebApiForDbUser($dbUser);
		$tracksFromImportPlaylists = [];
		foreach($importPlaylists as $importPlaylist) {
			$tracksFromImportPlaylists = array_merge($tracksFromImportPlaylists, $this->getTracksByPlaylist($importPlaylist['import_playlist_id']));
		}
		$tracksFromPlaylist = $this->getTracksByPlaylist($playlist['playlist_id']);
		$intersect = array_intersect_key($tracksFromPlaylist, $tracksFromImportPlaylists);
		$diff = array_diff_key($tracksFromPlaylist, $intersect);

		if($diff) {
			$this->api->deleteAllPlaylistTracks($playlist['playlist_id'], array_keys($diff));
		}
	}

	private function getImportPlaylistsForPlaylist($playlist) {
		$result = DatabaseUtility::getConnection()->query('SELECT * FROM apipi_import_playlists
			WHERE playlist_id = "'.$playlist['playlist_id'].'"
			and user_id = "'.$playlist['user_id'].'"
		');

		return $result->fetch_all(MYSQLI_ASSOC);
	}

	private function getPlaylistsForDeletion() {
		$dateToCheck = (new \DateTime())->modify('-'.self::MIN_DELAY_DAYS_FOR_CHECK.' day');
		return DatabaseUtility::getConnection()->query("
			SELECT playlist_id, user_id
			FROM apipi_playlists
			WHERE delete_tracks_if_not_in_import_playlists = 1
			AND 
				(last_time_tracks_deleted_not_in_import_playlists IS null
				or last_time_tracks_deleted_not_in_import_playlists <= '".$dateToCheck->format('Y-m-d H:i:s')."')
			ORDER BY last_time_tracks_deleted_not_in_import_playlists ASC
			LIMIT ".self::MAX_PLAYLIST_RUN."
		")->fetch_all(MYSQLI_ASSOC);
	}

	private function updateLastDeletionDate($playlist){
		DatabaseUtility::executeBindedQuery("UPDATE apipi_playlists 
			SET last_time_tracks_deleted_not_in_import_playlists = '".date('Y-m-d H:i:s')."'
			WHERE playlist_id = ?
			AND user_id = ?",
				"ss",
				[$playlist["playlist_id"], $playlist["user_id"]]
			);
	}
}