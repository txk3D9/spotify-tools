<?php

namespace SpotifyTools\Cronjobs;

use SpotifyTools\Cronjobs\AutomaticPlaylistBackup\BackupPlaylists;
use SpotifyTools\Cronjobs\AutomaticPlaylistsIntoPlaylistImporter\DeleteTracksWhichAreNotInImportPlaylists;
use SpotifyTools\Cronjobs\AutomaticPlaylistsIntoPlaylistImporter\ImportPlaylists;
use SpotifyTools\Cronjobs\SaveCurrentlyPlayingTrack\SaveCurrentlyPlayingTrack;
use SpotifyTools\Cronjobs\SaveRecentTracks\SaveRecentTracks;
use SpotifyTools\Cronjobs\GetTracks\GetTracks;
use SpotifyTools\Cronjobs\SaveTops\SaveTops;
use SpotifyTools\Services\AuthService;

abstract class AbstractCronjob {

	/**
	 * @var AuthService
	 */
	protected $authService;

	public function __construct() {
		$this->authService = new AuthService();
	}

	const CRONJOB_NAME_TO_CLASS = [
		//AutomaticPlaylistsIntoPlaylistImporter
		ImportPlaylists::CRONJOB_NAME => ImportPlaylists::class,
		DeleteTracksWhichAreNotInImportPlaylists::CRONJOB_NAME => DeleteTracksWhichAreNotInImportPlaylists::class,
		BackupPlaylists::CRONJOB_NAME => BackupPlaylists::class,
        SaveRecentTracks::CRONJOB_NAME => SaveRecentTracks::class,
        SaveCurrentlyPlayingTrack::CRONJOB_NAME => SaveCurrentlyPlayingTrack::class,
        SaveTops::CRONJOB_NAME => SaveTops::class,
        GetTracks::CRONJOB_NAME => GetTracks::class,
	];

	abstract function execute($args);
}