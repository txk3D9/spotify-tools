<?php

namespace SpotifyTools\Cronjobs\SaveCurrentlyPlayingTrack;

use SpotifyTools\Cronjobs\AbstractCronjob;
use SpotifyTools\Cronjobs\Traits\PlaylistHandlingTrait;
use SpotifyTools\Models\Log;
use SpotifyTools\Services\SpotifyWebApi;
use SpotifyTools\Utilities\AuthService;
use SpotifyTools\Utilities\DatabaseUtility;
use SpotifyTools\Utilities\DebugUtility;
use SpotifyTools\Utilities\LogUtility;
use SpotifyTools\Utilities\UserUtility;

class SaveCurrentlyPlayingTrack extends AbstractCronjob {

	const CRONJOB_NAME = 'saveCurrentlyPlayingTrack';
    /**
     * @var SpotifyWebApi
     */
    protected $api;

	function execute($args) {
        $users = UserUtility::getAllUsers();

        foreach($users as $user) {
            try {
                $this->api = $this->authService->getSpotifyWebApiForDbUser($user);
                $currentTrack = $this->api->getMyCurrentTrack();
                if($currentTrack->is_playing) {
                    $this->saveCurrentTrack($user, $currentTrack);
                }
            } catch (\Exception $e) {
                LogUtility::log(new Log(Log::ERROR,
                    "saveCurrentlyPlayingTrack", ["user_id" => $user['user_id'],
                        "error_message" => $e->getMessage()]));
            }
        }
	}

	private function saveCurrentTrack($user, $recentTrack) {
        DatabaseUtility::executeBindedQuery("INSERT INTO current_tracks_history 
			(user_id, track_id, `timestamp`, progress_ms)
			VALUES (?, ?, ?, ?)",
            "ssii",
            [$user['user_id'], $recentTrack->item->id, $recentTrack->timestamp, $recentTrack->progress_ms]);
    }
}