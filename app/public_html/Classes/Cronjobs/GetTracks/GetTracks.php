<?php

namespace SpotifyTools\Cronjobs\GetTracks;

use SpotifyTools\Actions\TopArtistsAndTracks\SaveTopTracksToPlaylist;
use SpotifyTools\Actions\TopArtistsAndTracks\TopArtistsAndTracks;
use SpotifyTools\Cronjobs\AbstractCronjob;
use SpotifyTools\Cronjobs\Traits\PlaylistHandlingTrait;
use SpotifyTools\Models\Log;
use SpotifyTools\Services\SpotifyWebApi;
use SpotifyTools\Utilities\AuthService;
use SpotifyTools\Utilities\DatabaseUtility;
use SpotifyTools\Utilities\DebugUtility;
use SpotifyTools\Utilities\LogUtility;
use SpotifyTools\Utilities\TemplateUtility;
use SpotifyTools\Utilities\UserUtility;

class GetTracks extends AbstractCronjob {

	const CRONJOB_NAME = 'getTracks';
    /**
     * @var SpotifyWebApi
     */
    protected $api;

	function execute($args) {
        $neededTrackIds = $this->getNeededTrackIds();
        if($neededTrackIds) {
            $user = UserUtility::getUserById(1191338807);
            $this->api = $this->authService->getSpotifyWebApiForDbUser($user);
            $tracks = $this->api->getTracks($neededTrackIds);
            file_put_contents("test.json", json_encode($tracks));
            foreach($tracks->tracks as $track) {
                $this->save($track);
            }
        }
	}

    private function getNeededTrackIds() {
        $result = DatabaseUtility::executeBindedQuery("
            SELECT cth.track_id 
            FROM current_tracks_history cth
            WHERE cth.track_id NOT IN (SELECT t.id FROM tracks t)
            GROUP BY cth.track_id
            LIMIT 50
        ", null, null);

        return array_map(function($a) {
            return $a[0];
        }, $result->fetch_all());
    }

	private function save($track) {
        DatabaseUtility::executeBindedQuery("INSERT INTO tracks 
			(id, name, artists, uri, image)
			VALUES (?, ?, ?, ?, ?)",
            "sssss",
            [$track->id, $track->name, json_encode($track->artists), $track->external_urls->spotify, $track->album->images[2]->url]
        );
    }
}