<?php

namespace SpotifyTools\Cronjobs\SaveRecentTracks;

use SpotifyTools\Cronjobs\AbstractCronjob;
use SpotifyTools\Cronjobs\Traits\PlaylistHandlingTrait;
use SpotifyTools\Models\Log;
use SpotifyTools\Services\SpotifyWebApi;
use SpotifyTools\Utilities\AuthService;
use SpotifyTools\Utilities\DatabaseUtility;
use SpotifyTools\Utilities\DebugUtility;
use SpotifyTools\Utilities\LogUtility;
use SpotifyTools\Utilities\UserUtility;
use SpotifyWebAPI\SpotifyWebAPIException;

class SaveRecentTracks extends AbstractCronjob {

	const CRONJOB_NAME = 'saveRecentTracks';
    /**
     * @var SpotifyWebApi
     */
    protected $api;

	function execute($args) {
        $users = UserUtility::getAllUsers();

        foreach($users as $user) {
            try {
                $this->api = $this->authService->getSpotifyWebApiForDbUser($user);
                $lastHistoryEntry = $this->getLastHistoryEntry($user);
                $afterCalcDate = $lastHistoryEntry ? $lastHistoryEntry['played_at'] : time()-60*60*24; //now minus 24 hrs
                $after = $lastHistoryEntry
                    ? (new \DateTime($afterCalcDate))->format('Uv')
                    : (new \DateTime())->setTimestamp(time()-60*60*24)->format('Uv');
                $recentTracks = $this->api->getMyRecentTracks(
                    ['limit' => 50, 'after' => $after, 'fields' => 'track(name,id),played_at,next,cursors']
                );
                foreach($recentTracks->items as $recentTrack) {
                    $this->saveHistory($user, $recentTrack);
                }
            } catch (\Exception $e) {
                LogUtility::log(new Log(Log::ERROR,
                    "saveRecentTracks", ["user_id" => $user['user_id'],
                        "error_message" => $e->getMessage()]));
            }
        }
	}

    private function getLastHistoryEntry($user) {
        $result = DatabaseUtility::executeBindedQuery("SELECT * FROM recent_tracks
            WHERE user_id = ?
            ORDER BY played_at DESC
            LIMIT 1",
            "s",
            [$user['user_id']]);

        return $result->fetch_assoc();

    }

	private function saveHistory($user, $history) {
        DatabaseUtility::executeBindedQuery("INSERT INTO recent_tracks 
			(user_id, track_id, played_at)
			VALUES (?, ?, ?)",
            "sss",
            [$user['user_id'], $history->track->id, str_replace('Z', '', $history->played_at)]);
    }
}