<?php

namespace SpotifyTools\Cronjobs\AutomaticPlaylistBackup;

use SpotifyTools\Cronjobs\AbstractCronjob;
use SpotifyTools\Cronjobs\Traits\PlaylistHandlingTrait;
use SpotifyTools\Services\SpotifyWebApi;
use SpotifyTools\Utilities\AuthService;
use SpotifyTools\Utilities\DatabaseUtility;
use SpotifyTools\Utilities\DebugUtility;
use SpotifyTools\Utilities\UserUtility;

class BackupPlaylists extends AbstractCronjob {

	const CRONJOB_NAME = 'apb_backupPlaylists';
	use PlaylistHandlingTrait;

	function execute($args) {
		$backupPlaylists = $this->getDueBackupPlaylists();
		if ($backupPlaylists) {
			foreach ($backupPlaylists as $backupPlaylist) {
				$dbUser = UserUtility::getUserById($backupPlaylist['user_id']);
				$this->api = $this->authService->getSpotifyWebApiForDbUser($dbUser);
				$this->backupPlaylist($backupPlaylist);
				$this->updateLastBackupDate($backupPlaylist);
			}
		}
	}

	private function backupPlaylist($backupPlaylist) {
		$playlistId = $backupPlaylist['playlist_id'];
		$playlistTracks = $this->getTracksByPlaylist($playlistId);
		if ($playlistTracks) {
			$newPlaylistName = $this->createPlaylistNameFromScheme($backupPlaylist['backup_name_scheme']);
			$newPlaylist = $this->api->createPlaylist(['name' => $newPlaylistName, 'public' => false]);
			$this->api->addAllPlaylistTracks($newPlaylist->id, array_keys($playlistTracks));
		}
	}

	private function createPlaylistNameFromScheme($playlistNameScheme) {
		return str_replace(
			["%y", "%m", "%d", "%w"],
			[date("Y"), date("m"), date("d"), date("W")],
			$playlistNameScheme
		);
	}

	private function getDueBackupPlaylists() {
		$todaysWeekDay = strtolower(date('l'));
		$result = DatabaseUtility::getConnection()->query('SELECT * FROM apb_playlists
			WHERE '.$todaysWeekDay.' = 1
			AND backup_time <= "'.date('H:i:s').'"
			AND (
				last_backup IS NULL 
					OR
				last_backup < CONCAT("'.date('Y-m-d').'", " ", backup_time)
				)
			ORDER BY user_id
			');

		return $result->fetch_all(MYSQLI_ASSOC);
	}

	private function updateLastBackupDate($backupPlaylist) {
		DatabaseUtility::executeBindedQuery("UPDATE apb_playlists 
			SET last_backup = '".date('Y-m-d H:i:s')."'
			WHERE playlist_id = ?
			AND user_id = ?",
			"ss",
			[$backupPlaylist["playlist_id"], $backupPlaylist["user_id"]]
		);
	}
}