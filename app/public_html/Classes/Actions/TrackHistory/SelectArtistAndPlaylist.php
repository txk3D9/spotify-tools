<?php

namespace SpotifyTools\Actions\AllTracksByArtist;

use SpotifyTools\Actions\AbstractAction;
use SpotifyTools\Utilities\DebugUtility;
use SpotifyTools\Utilities\SessionUtility;
use SpotifyTools\Utilities\TemplateUtility;
use SpotifyTools\Utilities\UserUtility;

class SelectArtistAndPlaylist extends AbstractAction {

	public const ACTION_NAME = 'allTracksByArtist_selectArtistAndPlaylist';

	public function execute() {
		if(!array_key_exists('artist', $_GET) || !$_GET['artist']) {
			throw new \Exception('No artist selected');
		}
		$artistName = $_GET['artist'];
		$artistsResult = $this->api->search($artistName, "artist", ["limit" => 8]);
		$artists = [];
		if($artistsResult->artists->items && count($artistsResult->artists->items) > 0) {
			foreach($artistsResult->artists->items as $artist) {
				$artists[] = $artist;
			}
		}
		$playlists = $this->api->getAllMyPlaylists();
		echo TemplateUtility::getHtml('AllTracksByArtist/SelectArtistAndPlaylist', [
			'playlists' => $playlists,
			'artists' => $artists,
			'action' => Submit::ACTION_NAME,
		]);
	}
}