<?php

namespace SpotifyTools\Actions;

use SpotifyTools\Utilities\TemplateUtility;
use SpotifyTools\Utilities\UserUtility;

class Overview extends AbstractAction {

	const ACTION_NAME = 'overview';

	public function execute() {
		echo TemplateUtility::getHtml('Overview', [
			'user' => UserUtility::getSpotifyUser($this->api),
			'actions' => [
				AllTracksByArtist\SearchArtist::ACTION_NAME => [
					'name' => 'Get all Tracks from one Artist',
					'description' => 'Select an artist and add all of its tracks to one playlist',
					'id' => 'allTracksByArtist'
				],
				ArtistTracksFromPlaylistToPlaylist\SelectPlaylist::ACTION_NAME => [
					'name' => 'Copy Artist Tracks From Playlist to new Playlist',
					'description' => 'Select artists from one playlist and copy the songs of those artists to a new playlist',
					'id' => 'artistTracksFromPlaylistToPlaylist'
				],
				AutomaticPlaylistsIntoPlaylistImporter\PlaylistsOverview::ACTION_NAME => [
					'name' => 'Automatic Playlists into Playlist importer',
					'description' => 'Select playlists that should automatically be imported into another playlist',
					'id' => 'automaticPlaylistsIntoPlaylistImporter'
				],
				AutomaticPlaylistBackup\Settings::ACTION_NAME => [
					'name' => 'Automatic Playlist Backup',
					'description' => 'Setup automatic backups for your playlists. Usefull f.e. for Discover Weekly / Release Radar',
					'id' => 'automaticPlaylistBackup'
				],
				RemoveDuplicateTracksFromPlaylist\SelectPlaylist::ACTION_NAME => [
					'name' => 'Remove duplicate Tracks from Playlist',
					'description' => 'Select a playlist, check all tracks that could be duplicate and remove them',
					'id' => 'removeDuplicateTracksFromPlaylist'
				],
				RemoveTracksFromPlaylistByLikedTracks\SelectPlaylist::ACTION_NAME => [
					'name' => 'Remove Tracks from Playlist by liked Songs',
					'description' => 'Remove all Tracks in a playlist that you have in your "liked songs"',
					'id' => 'removeTracksFromPlaylistByLikedTracks'
				],
				TopArtistsAndTracks\TopArtistsAndTracks::ACTION_TOP_TRACKS => [
					'name' => 'Top Tracks',
					'id' => 'topTracks'
				],
				TopArtistsAndTracks\TopArtistsAndTracks::ACTION_TOP_ARTISTS => [
					'name' => 'Top Artists',
					'id' => 'topArtists'
				],
                DuplicateTracksOfPlaylists\Select::ACTION_NAME => [
                    'name' => 'Duplicate Tracks of Playlists',
                    'description' => 'Get duplicate Tracks of 2 Playlists and copy them to a new playlist',
                    'id' => 'duplicateTracksOfPlaylists'
                ],
                TrackHistory\View::ACTION_NAME => [
                    'name' => 'Track History',
                    'id' => 'trackHistory'
                ],
			],
		]);
	}
}