<?php

namespace SpotifyTools\Actions\DuplicateTracksOfPlaylists;

use SpotifyTools\Actions\AbstractAction;
use SpotifyTools\Utilities\TemplateUtility;

class Select extends AbstractAction {

	public const ACTION_NAME = 'DuplicateTracksOfPlaylists_select';

	public function execute() {
		$playlists = $this->api->getAllMyPlaylists(['fields' => ['id,description,name']]);
		echo TemplateUtility::getHtml('DuplicateTracksOfPlaylists/Select', [
			'playlists' => $playlists,
			'action' => Submit::ACTION_NAME,
		]);
	}
}