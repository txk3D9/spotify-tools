<?php

namespace SpotifyTools\Actions\DuplicateTracksOfPlaylists;

use SpotifyTools\Actions\AbstractAction;
use SpotifyTools\Services\DuplicateTracksService;
use SpotifyTools\Utilities\TemplateUtility;

class Submit extends AbstractAction {

	public const ACTION_NAME = 'DuplicateTracksOfPlaylists_submit';

	public function execute() {
		$playlistIds = [
			$_POST['playlist-input-1'] ? trim($_POST['playlist-input-1']) : $_POST['playlist-select-1'],
			$_POST['playlist-input-2'] ? trim($_POST['playlist-input-2']) : $_POST['playlist-select-2'],
		];
		$resultTrackIds = [];
		try {
			$tracks = [];
			$duplicateTracksService = new DuplicateTracksService();
			foreach ($playlistIds as $playlistId) {
				$request = $this->api->getAllPlaylistTracks($playlistId, ['fields' => DuplicateTracksService::SELECT_FIELDS]);
				//we need to remove all duplicates from each playlist, so we can group them together afterwards...
				$uniqueTracks = $duplicateTracksService->removeDuplicates($request->items);
				$tracks = array_merge(array_values($tracks), array_values($uniqueTracks));
			}
			$uniqueDuplicates = $duplicateTracksService->getUniqueDuplicates($tracks);
			$resultTrackIds = array_keys($uniqueDuplicates);
			if ($resultTrackIds) {
				$newPlaylistResponse = $this->api->createPlaylist(['name' => $_POST['new-playlist-name'], 'public' => false]);
				$this->api->addAllPlaylistTracks($newPlaylistResponse->id, $resultTrackIds);
			}
		} catch (\Exception $e) {

		}
		echo TemplateUtility::getHtml('DuplicateTracksOfPlaylists/Submit', [
			'newPlaylistName' => $_POST['new-playlist-name'],
			'success' => (bool) $resultTrackIds
		]);
	}
}