<?php

namespace SpotifyTools\Actions\RemoveTracksFromPlaylistByLikedTracks;

use SpotifyTools\Actions\AbstractAction;
use SpotifyTools\Utilities\PlaylistsUtility;
use SpotifyTools\Utilities\SessionUtility;
use SpotifyTools\Utilities\TemplateUtility;
use SpotifyTools\Utilities\UserUtility;

class SelectPlaylist extends AbstractAction {

	public const ACTION_NAME = 'RemoveTracksFromPlaylistByLikedTracks_selectPlaylist';

	public function execute() {
		$playlists = $this->api->getAllMyPlaylists(['fields' => ['id,description,name']]);
		$playlists = PlaylistsUtility::filterPlaylistsByOwner($playlists, UserUtility::getSpotifyUser($this->api));
		echo TemplateUtility::getHtml('RemoveTracksFromPlaylistByLikedTracks/SelectPlaylist', [
			'playlists' => $playlists,
			'action' => Submit::ACTION_NAME,
		]);
	}
}