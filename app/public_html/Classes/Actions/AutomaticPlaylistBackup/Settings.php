<?php

namespace SpotifyTools\Actions\AutomaticPlaylistBackup;

use SpotifyTools\Actions\AbstractAction;
use SpotifyTools\Models\AutomaticPlaylistBackup\PlaylistContainer;
use SpotifyTools\Utilities\DatabaseUtility;
use SpotifyTools\Utilities\SessionUtility;
use SpotifyTools\Utilities\TemplateUtility;
use SpotifyTools\Utilities\UserUtility;

class Settings extends AbstractAction {

	public const ACTION_NAME = 'AutomaticPlaylistBackup_settings';

	public const BACKUP_DATES = [
		'monday' => 'Mon',
		'tuesday' => 'Tue',
		'wednesday' => 'Wen',
		'thursday' => 'Thu',
		'friday' => 'Fri',
		'saturday' => 'Sat',
		'sunday' => 'Son',
	];

	public function execute() {
		$databasePlaylist = $this->getPlaylistFromUser(UserUtility::getSpotifyUser($this->api));
		$spotifyPlaylists = $this->api->getAllMyPlaylists();
		$playlistContainers = $this->wrapPlaylistsInContainer($spotifyPlaylists, $databasePlaylist);
		echo TemplateUtility::getHtml('AutomaticPlaylistBackup/Settings', [
			'playlistContainers' => $playlistContainers,
			'databasePlaylist' => $databasePlaylist,
			'backupDates' => Settings::BACKUP_DATES,
			'action' => Save::ACTION_NAME,
		]);
	}

	private function getPlaylistFromUser($user) {
		$result = DatabaseUtility::executeBindedQuery("
			SELECT * FROM apb_playlists 
			WHERE user_id = ?",
			"s",
			[$user->id]);

		return $result->fetch_all(MYSQLI_ASSOC);
	}

	/**
	 * @param $spotifyPlaylists
	 * @param $playlistsFromDatabase
	 * @return PlaylistContainer[]
	 */
	protected function wrapPlaylistsInContainer($spotifyPlaylists, $playlistsFromDatabase) {
		$playlistContainers = [];
		foreach ($spotifyPlaylists->items as $spotifyPlaylist) {
			$playlistContainer = new PlaylistContainer();
			$playlistContainer->setSpotifyPlaylist($spotifyPlaylist);

			if($playlistsFromDatabase) {
				foreach ($playlistsFromDatabase as $key => $playlistFromDatabase) {
					if ($playlistFromDatabase["playlist_id"] == $spotifyPlaylist->id) {
						$playlistContainer->setDatabasePlaylist($playlistFromDatabase);
						unset($playlistsFromDatabase[$key]);
						break;
					}
				}
			}
			$playlistContainers[] = $playlistContainer;
		}

		return $playlistContainers;
	}
}