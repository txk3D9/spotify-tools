<?php

namespace SpotifyTools\Actions\TopArtistsAndTracks;

use SpotifyTools\Actions\AbstractAction;
use SpotifyTools\Utilities\DebugUtility;
use SpotifyTools\Utilities\SortingUtility;
use SpotifyTools\Utilities\TemplateUtility;
use SpotifyWebAPI\SpotifyWebAPIException;

class TopArtistsAndTracks extends AbstractAction {

	const KIND_ARTISTS = 'artists';
	const KIND_TRACKS = 'tracks';
	const ACTION_TOP_TRACKS = 'topTracks';
	const ACTION_TOP_ARTISTS = 'topArtists';
	const KINDS_BY_ACTION = [
		self::ACTION_TOP_ARTISTS => self::KIND_ARTISTS,
		self::ACTION_TOP_TRACKS => self::KIND_TRACKS
	];
	const TIME_RANGE_LONG_TERM = 'long_term';
	const TIME_RANGE_MEDIUM_TERM = 'medium_term';
	const TIME_RANGE_SHORT_TERM = 'short_term';
	const TIME_RANGES = [
		self::TIME_RANGE_LONG_TERM => 'All time',
		self::TIME_RANGE_MEDIUM_TERM => '6 Months',
		self::TIME_RANGE_SHORT_TERM => '4 Weeks',
	];

	public function execute() {
		$kind = $this->getKindByAction();
		$tops = null;
		$timeRange = null;
		if (array_key_exists('time_range', $_GET)) {
			$timeRange = $_GET['time_range'];
			if (!array_key_exists($timeRange, self::TIME_RANGES)) {
				throw new \Exception('Invalid time range');
			}
			$tops = $this->getTopsByKindAndTimeRange($kind, $timeRange);
		}
		echo TemplateUtility::getHtml('TopArtistsAndTracks/Top'.ucfirst($kind), [
			'action' => $_GET['action'],
			'saveAction' => SaveTopTracksToPlaylist::ACTION_NAME,
			'tops' => $tops,
			'timeRanges' => self::TIME_RANGES,
			'selectedTimeRange' => $timeRange
		]);
	}

	protected function getKindByAction() {
		$action = $_GET['action'];
		if (array_key_exists($action, self::KINDS_BY_ACTION)) {
			return self::KINDS_BY_ACTION[$action];
		}
		throw new \Exception('no kind for action "'.$action);
	}

	private function getTopsByKindAndTimeRange($kind, $timeRange) {
		if ($kind == self::KIND_ARTISTS) {
			return $this->api->getTopArtists($timeRange);
		} else if ($kind == self::KIND_TRACKS) {
			return $this->api->getTopTracks($timeRange);
		}
	}
}