<?php
namespace SpotifyTools\Actions\AllTracksByArtist;

use SpotifyTools\Actions\AbstractAction;
use SpotifyTools\Utilities\DebugUtility;
use SpotifyTools\Utilities\SortingUtility;
use SpotifyTools\Utilities\TemplateUtility;
use SpotifyWebAPI\SpotifyWebAPIException;

class SearchArtist extends AbstractAction {

	public const ACTION_NAME = 'allTracksByArtist_searchArtist';

	public function execute() {
		echo TemplateUtility::getHtml('AllTracksByArtist/SearchArtist', [
			'action' => SelectArtistAndPlaylist::ACTION_NAME,
		]);
	}
}