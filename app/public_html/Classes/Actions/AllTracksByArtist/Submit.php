<?php

namespace SpotifyTools\Actions\AllTracksByArtist;

use SpotifyTools\Actions\AbstractAction;
use SpotifyTools\Utilities\DebugUtility;
use SpotifyTools\Utilities\TemplateUtility;

class Submit extends AbstractAction {

	public const ACTION_NAME = 'allTracksByArtist_submit';

	public function execute() {
		if (!array_key_exists('artist', $_REQUEST)
			|| !$_REQUEST['artist']
			|| (!array_key_exists('new-playlist-name', $_REQUEST) && !array_key_exists('playlist', $_REQUEST))
			|| (!$_REQUEST['new-playlist-name'] && !$_REQUEST['playlist'])) {
			throw new \Exception('Not all needed parameters are set. '.self::class);
		}
		$artistId = $_REQUEST['artist'];
		$artist = $this->api->getArtist($artistId);
		$tracks = array_merge(
			$this->getNormalTracks($artist, (bool) $_REQUEST['include-remixes-of-other-artists']),
			$this->getRemixTracks($artist),
		);
		$newPlaylistName = $_REQUEST['new-playlist-name'];
		if ($tracks) {
			$tracks = $this->sortTracksByName(
				$this->filterTracks($tracks)
			);
			$this->addTracksToPlaylist($tracks, $_REQUEST['playlist'], $newPlaylistName);
		}
		echo TemplateUtility::getHtml('AllTracksByArtist/Submit', [
			'newPlaylistName' => $newPlaylistName,
		]);
	}

	private function getNormalTracks($artist, $includeRemixesOfOtherArtists = false) {
		$artistId = $artist->id;
		$tracks = [];
		$albums = $this->api->getAllArtistAlbums($artistId);
		foreach ($albums->items as $album) {
			$tracksOfAlbum = $this->api->getAllAlbumTracks($album->id);
			foreach ($tracksOfAlbum->items as $track) {
				$isCorrectArtist = false;
				foreach ($track->artists as $artist) {
					if ($artist->id == $artistId) {
						$isCorrectArtist = true;
						break;
					}
				}
				if ($isCorrectArtist
					&& ($includeRemixesOfOtherArtists || !$this->isRemixFromOtherArtist($artist, $track))
					&& !array_key_exists($track->id, $tracks)) {
					$tracks[$track->id] = $track;
				}
			}
		}

		return $tracks;
	}

	private function isRemixFromOtherArtist($artist, $track) {
		return (strpos(strtolower($track->name), 'remix') !== false) && (strpos(strtolower($track->name), $artist->name) === false);
	}

	private function getRemixTracks($artist) {
		$artistName = $artist->name;
		$tracks = [];
		if ($artistName) {
			$result = $this->api->allTracksSearch($artistName.' Remix');
			foreach ($result->tracks->items as $track) {
				if (!array_key_exists($track->id, $tracks)) {
					$tracks[$track->id] = $track;
				}
			}
			foreach ($tracks as $trackId => $track) {
				if (strstr(strtolower($track->name), strtolower($artistName)) === false) {
					unset($tracks[$trackId]);
				}
			}
		}

		return $tracks;
	}

	private function filterTracks($tracks) {
		$trackNames = [];
		$filteredTracks = [];
		foreach ($tracks as $track) {
			if (!array_key_exists($track->name, $trackNames)) {
				$filteredTracks[] = $track;
				$trackNames[$track->name] = $track;
			}
		}

		return $filteredTracks;
	}

	private function sortTracksByName($tracks) {
		usort($tracks, function ($track1, $track2) {
			return $track1->name > $track2->name;
		});

		return $tracks;
	}

	private function addTracksToPlaylist($tracks, $playlistId, $newPlaylistName = '') {
		if ($newPlaylistName) {
			$newPlaylistResponse = $this->api->createPlaylist(['name' => $newPlaylistName, 'public' => false]);
			$insertPlaylistId = $newPlaylistResponse->id;
		} else {
			$insertPlaylistId = $playlistId;
		}
		if ($tracks) {
			$trackIdsToAdd = [];
			foreach ($tracks as $track) {
				$trackIdsToAdd[] = $track->id;
			}
			$this->api->addAllPlaylistTracks($insertPlaylistId, $trackIdsToAdd);
		}
	}
}