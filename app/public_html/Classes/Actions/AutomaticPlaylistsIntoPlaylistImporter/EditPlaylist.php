<?php

namespace SpotifyTools\Actions\AutomaticPlaylistsIntoPlaylistImporter;

use SpotifyTools\Actions\AbstractAction;
use SpotifyTools\Models\AutomaticPlaylistsIntoPlaylistImporter\PlaylistContainer;
use SpotifyTools\Utilities\DatabaseUtility;
use SpotifyTools\Utilities\DebugUtility;
use SpotifyTools\Utilities\SessionUtility;
use SpotifyTools\Utilities\TemplateUtility;
use SpotifyTools\Utilities\UserUtility;

class EditPlaylist extends AbstractAction {

	public const ACTION_NAME = 'AutomaticPlaylistsIntoPlaylistImporter_editPlaylist';

	public const IMPORT_DATES = [
			'monday' => 'Mon',
			'tuesday' => 'Tue',
			'wednesday' => 'Wen',
			'thursday' => 'Thu',
			'friday' => 'Fri',
			'saturday' => 'Sat',
			'sunday' => 'Son',
		];

	public function execute() {
		$playlistId = $_GET['playlist'];
		$databaseImportPlaylists = $this->getImportPlaylistsFromDatabase(UserUtility::getSpotifyUser($this->api), $playlistId);
		$databasePlaylist = $this->getPlaylistFromUser(UserUtility::getSpotifyUser($this->api), $playlistId);
		$spotifyPlaylist = $this->api->getPlaylist($playlistId);
		$spotifyPlaylists = $this->api->getAllMyPlaylists();
		$playlistContainers = $this->wrapPlaylistsInContainer($spotifyPlaylists, $databaseImportPlaylists);
		foreach($playlistContainers as $key => $playlistContainer) {
			if($playlistContainer->getSpotifyPlaylist()->id == $playlistId) {
				unset($playlistContainers[$key]);
				break;
			}
		}
		echo TemplateUtility::getHtml('AutomaticPlaylistsIntoPlaylistImporter/EditPlaylist', [
			'playlistContainers' => $playlistContainers,
			'spotifyPlaylist' => $spotifyPlaylist,
			'databasePlaylist' => $databasePlaylist,
			'importDates' => EditPlaylist::IMPORT_DATES,
			'action' => Save::ACTION_NAME,
		]);
	}

	private function getPlaylistFromUser($user, $playlistId) {
		$result = DatabaseUtility::executeBindedQuery("
			SELECT * FROM apipi_playlists 
			WHERE user_id = ?
			AND playlist_id = ?",
			"ss",
			[$user->id, $playlistId]);

		return $result->fetch_assoc();
	}

	private function getImportPlaylistsFromDatabase($user, $playlistId) {
		$result = DatabaseUtility::executeBindedQuery("
			SELECT * FROM apipi_import_playlists 
			WHERE user_id = ?
			AND playlist_id = ?",
			"ss",
			[$user->id, $playlistId]);

		return $result->fetch_all(MYSQLI_ASSOC);
	}

	/**
	 * @param $spotifyPlaylists
	 * @param $playlistsFromDatabase
	 * @return PlaylistContainer[]
	 */
	protected function wrapPlaylistsInContainer($spotifyPlaylists, $playlistsFromDatabase) {
		$playlistContainers = [];
		foreach ($spotifyPlaylists->items as $spotifyPlaylist) {
			$playlistContainer = new PlaylistContainer();
			$playlistContainer->setSpotifyPlaylist($spotifyPlaylist);
			if($playlistsFromDatabase) {
				foreach ($playlistsFromDatabase as $key => $playlistFromDatabase) {
					if ($playlistFromDatabase["import_playlist_id"] == $spotifyPlaylist->id) {
						$playlistContainer->setDatabaseImportPlaylist($playlistFromDatabase);
						unset($playlistsFromDatabase[$key]);
						break;
					}
				}
			}
			$playlistContainers[] = $playlistContainer;
		}

		return $playlistContainers;
	}
}