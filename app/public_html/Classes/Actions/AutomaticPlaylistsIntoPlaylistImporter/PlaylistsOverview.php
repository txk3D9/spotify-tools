<?php
namespace SpotifyTools\Actions\AutomaticPlaylistsIntoPlaylistImporter;

use SpotifyTools\Actions\AbstractAction;
use SpotifyTools\Utilities\DatabaseUtility;
use SpotifyTools\Utilities\PlaylistsUtility;
use SpotifyTools\Utilities\DebugUtility;
use SpotifyTools\Utilities\SortingUtility;
use SpotifyTools\Utilities\TemplateUtility;
use SpotifyTools\Utilities\UserUtility;
use SpotifyWebAPI\SpotifyWebAPIException;

class PlaylistsOverview extends AbstractAction {

	public const ACTION_NAME = 'AutomaticPlaylistsIntoPlaylistImporter_playlistsOverview';

	public function execute() {
		$configuredPlaylistIds = $this->getConfiguredPlaylistIds(UserUtility::getSpotifyUser($this->api));
		$spotifyPlaylists = $this->api->getAllMyPlaylists();
		$spotifyPlaylists = PlaylistsUtility::filterPlaylistsByOwner($spotifyPlaylists, UserUtility::getSpotifyUser($this->api));
		echo TemplateUtility::getHtml('AutomaticPlaylistsIntoPlaylistImporter/PlaylistsOverview', [
			'spotifyPlaylists' => $spotifyPlaylists,
			'configuredPlaylistIds' => $configuredPlaylistIds,
			'action' => EditPlaylist::ACTION_NAME,
		]);
	}

	private function getConfiguredPlaylistIds($user) {
		$result = DatabaseUtility::executeBindedQuery("
			SELECT playlist_id FROM apipi_playlists 
			WHERE user_id = ?
			GROUP BY playlist_id",
			"s",
			[$user->id]
		);

		$configuredPlaylistIds = [];
		if($result) {
			$rows = $result->fetch_all(MYSQLI_ASSOC);
			if($rows) {
				foreach($rows as $row) {
					$configuredPlaylistIds[$row['playlist_id']] = $row['playlist_id'];
				}
			}
		}

		return $configuredPlaylistIds;
	}
}