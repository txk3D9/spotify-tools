<?php

namespace SpotifyTools\Actions\ArtistTracksFromPlaylistToPlaylist;

use SpotifyTools\Actions\AbstractAction;
use SpotifyTools\Utilities\SessionUtility;
use SpotifyTools\Utilities\TemplateUtility;
use SpotifyTools\Utilities\UserUtility;

class SelectPlaylist extends AbstractAction {

	public const ACTION_NAME = 'artistTracksFromPlaylistToPlaylist_selectPlaylist';

	public function execute() {
		$playlists = $this->api->getAllMyPlaylists();
		echo TemplateUtility::getHtml('ArtistTracksFromPlaylistToPlaylist/SelectPlaylist', [
			'playlists' => $playlists,
			'action' => SelectArtists::ACTION_NAME,
		]);
	}
}