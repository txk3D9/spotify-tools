<?php
namespace SpotifyTools\Actions\ArtistTracksFromPlaylistToPlaylist;

use SpotifyTools\Actions\AbstractAction;
use SpotifyTools\Repositories\PlaylistRepository;
use SpotifyTools\Utilities\DebugUtility;
use SpotifyTools\Utilities\SortingUtility;
use SpotifyTools\Utilities\TemplateUtility;
use SpotifyWebAPI\SpotifyWebAPIException;

class SelectArtists extends AbstractAction {

	public const ACTION_NAME = 'artistTracksFromPlaylistToPlaylist_selectArtists';

	public function execute() {
		if(!array_key_exists('playlist', $_GET)) {
			throw new \Exception('No playlist id set');
		}
		$playlistId = $_GET['playlist'];
		$playlist = PlaylistRepository::getPlaylist($playlistId, $this->api);
		$artists = [];
		foreach($playlist->tracks as $track) {
			foreach($track->track->artists as $artist) {
				if(!array_key_exists($artist->id, $artists)) {
					$artists[$artist->id] = $artist;
				}
			}
		}
		$artists = SortingUtility::sortArtistsByName($artists);
		echo TemplateUtility::getHtml('ArtistTracksFromPlaylistToPlaylist/SelectArtists', [
			'playlist' => $playlist,
			'artists' => $artists,
			'action' => Submit::ACTION_NAME,
		]);
	}
}