<?php
namespace SpotifyTools\Actions\ArtistTracksFromPlaylistToPlaylist;

use SpotifyTools\Actions\AbstractAction;
use SpotifyTools\Models\Log;
use SpotifyTools\Repositories\PlaylistRepository;
use SpotifyTools\Utilities\DebugUtility;
use SpotifyTools\Utilities\LogUtility;
use SpotifyTools\Utilities\TemplateUtility;

class Submit extends AbstractAction {

	public const ACTION_NAME = 'artistTracksFromPlaylistToPlaylist_submit';

	public function execute() {
		if(!array_key_exists('playlist-id', $_REQUEST) ||
			!array_key_exists('new-playlist-name', $_REQUEST) ||
			!array_key_exists('artistsJson', $_REQUEST)
			|| !$_REQUEST['playlist-id']
			|| !$_REQUEST['new-playlist-name']
			|| !$_REQUEST['artistsJson']) {
			throw new \Exception('Not all needed parameters are set. '.self::class);
		}
		$playlistId = $_REQUEST['playlist-id'];
		$newPlaylistName = $_REQUEST['new-playlist-name'];
		$artistIdsInKeys = array_flip(explode(',', $_REQUEST['artistsJson']));
        LogUtility::log(new Log('info',
            'ATFPTP Submit getPlaylist',
            ['playlistId' => $playlistId, 'artists' => $artistIdsInKeys]));
        $playlist = PlaylistRepository::getPlaylist($playlistId, $this->api);
		$trackIdsToAdd = [];
		foreach($playlist->tracks as $track) {
			foreach($track->track->artists as $artist) {
				if(array_key_exists($artist->id, $artistIdsInKeys)
					&& !array_key_exists($track->track->id, $trackIdsToAdd)) {
					$trackIdsToAdd[$track->track->id] = $track->track->id;
					break;
				}
			}
		}
        LogUtility::log(new Log('info',
            'ATFPTP Submit tracks that should be added', ['trackIds' => $trackIdsToAdd]));
		$newPlaylistResponse = $this->api->createPlaylist(['name' => $newPlaylistName, 'public' => false]);
        LogUtility::log(new Log('info',
            'ATFPTP Submit add tracks to playlist start',
            ['newPlaylistId' => $newPlaylistResponse->id, 'trackIds' => $trackIdsToAdd]));
		$this->api->addAllPlaylistTracks($newPlaylistResponse->id, $trackIdsToAdd);
        LogUtility::log(new Log('info', 'ATFPTP Submit add tracks to playlist end'));
		echo TemplateUtility::getHtml('ArtistTracksFromPlaylistToPlaylist/Submit', [
			'newPlaylistName' => $newPlaylistName,
		]);
	}
}