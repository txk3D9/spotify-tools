<?php

namespace SpotifyTools\Utilities;

use andi3\ServerControlPanel\Models\Applications\Application;
use andi3\ServerControlPanel\Models\Config;
use andi3\ServerControlPanel\Models\Server;
use Twig\Extra\Intl\IntlExtension;

class TemplateUtility {

	public static function getHtml(string $template, array $arguments = []): string {
		$loader = new \Twig\Loader\FilesystemLoader([
			dirname(__FILE__).'/../../Resources/Private/Layouts/',
			dirname(__FILE__).'/../../Resources/Private/Partials/',
			dirname(__FILE__).'/../../Resources/Private/Templates/'
		]);
		$twig = new \Twig\Environment($loader, [
			'debug' => true,
//			'cache' => dirname(__FILE__).'/../../cache',
		]);
		$twig->addExtension(new \Twig\Extension\DebugExtension());

		if(!array_key_exists('user', $arguments)) {
			$arguments['user'] = UserUtility::getUserFromPHPSession();
		}

		return $twig->render($template.'.html.twig', $arguments);
	}
}