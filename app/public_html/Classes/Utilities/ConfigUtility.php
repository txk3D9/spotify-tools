<?php

namespace SpotifyTools\Utilities;

use SpotifyTools\Models\Config;

class ConfigUtility
{

    const CONFIG_FILE = '../config.json';
    /**
     * @var Config
     */
    private static $config;

    /**
     * @return Config
     */
    public static function getConfig()
    {
        if (ConfigUtility::$config === null) {
            $configFile = json_decode(file_get_contents(dirname(__FILE__).'/../../'.ConfigUtility::CONFIG_FILE), true);
            $config = new Config();
            $config->setClientId($configFile['client-id']);
            $config->setClientSecret($configFile['client-secret']);
			$config->setBaseUrl($configFile['base-url']);
			$config->setDatabase($configFile['database']);
            ConfigUtility::$config = $config;
        }

        return ConfigUtility::$config;
    }
}