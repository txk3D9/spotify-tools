<?php

namespace SpotifyTools\Utilities;

use SpotifyTools\Services\SpotifyWebApi;
use SpotifyWebAPI\Session;

class DatabaseUtility {

	/**
	 * @var \mysqli
	 */
	private static $connection;

	public static function connect() {
		$config = ConfigUtility::getConfig();
		$databaseConfig = $config->getDatabase();
		DatabaseUtility::$connection = mysqli_connect($databaseConfig['host'], $databaseConfig['username'], $databaseConfig['password'], $databaseConfig['database']);
		if (!DatabaseUtility::$connection) {
			throw new \Exception('Database connection could not be established');
		}
	}

	public static function executeBindedQuery($query, $paramTypes, $params) {
		$stmt = DatabaseUtility::getConnection()->prepare($query);
		if($stmt) {
            if($paramTypes) {
                $stmt->bind_param($paramTypes, ...$params);
            }
			$stmt->execute();
			$result = $stmt->get_result();
			$stmt->close();
			return $result;
		} else {
			throw new \Exception('Error creating statement in DatabaseUtility::executeBindedQuery');
		}

	}

	public static function getConnection() {
		if(!DatabaseUtility::$connection) {
			throw new \Exception('Database not connected');
		}
		return DatabaseUtility::$connection;
	}

	public static function closeConnection() {
		if(!DatabaseUtility::$connection) {
			throw new \Exception('Database not connected');
		}
		mysqli_close(DatabaseUtility::$connection);
	}
}