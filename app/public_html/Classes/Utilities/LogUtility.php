<?php

namespace SpotifyTools\Utilities;

use SpotifyTools\Models\Config;
use SpotifyTools\Models\Log;

class LogUtility {

	private static $requestId;

	public static function log(Log $log) {
		$log->setRequestId(LogUtility::getUniqueLogIdForCurrentRequest());
		try {
			$log->setUserId(UserUtility::getUserFromPHPSession()["user_id"]);
		} catch (\Exception $e) {
		}

		DatabaseUtility::executeBindedQuery("INSERT INTO logs 
			(user_id, request_id, log_level, message, data, created_at)
			VALUES (?, ?, ?, ?, ?, now())",
			"sssss",
			[$log->getUserId(), $log->getRequestId(), $log->getLogLevel(), $log->getMessage(), json_encode($log->getData())]);
	}

	public static function getUniqueLogIdForCurrentRequest() {
		if (LogUtility::$requestId == null) {
			LogUtility::$requestId = uniqid();
		}

		return LogUtility::$requestId;
	}
}