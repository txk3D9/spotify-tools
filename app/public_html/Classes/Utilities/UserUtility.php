<?php

namespace SpotifyTools\Utilities;

use http\Client\Curl\User;
use SpotifyTools\Services\SpotifyWebApi;

class UserUtility {

	private static $spotifyUser;
	private static $databaseUser;
	private static $databaseUsers = [];

	public static function getSpotifyUser(SpotifyWebApi $api) {
		if (UserUtility::$spotifyUser === null) {
			UserUtility::$spotifyUser = $api->me();
		}

		return UserUtility::$spotifyUser;
	}

    /**
     * @throws \Exception
     */
	public static function getUserFromPHPSession() {
		if (UserUtility::$databaseUser) {
			return UserUtility::$databaseUser;
		}
		$result = DatabaseUtility::getConnection()->query("SELECT * FROM users WHERE php_session_id = '".session_id()."'");
		if ($result) {
			UserUtility::$databaseUser = $result->fetch_assoc();
			return UserUtility::$databaseUser;
		} else {
			throw new \Exception('User not found from php session');
		}
	}

    public static function getUserById($userId) {
        if (array_key_exists($userId, UserUtility::$databaseUsers)) {
            return UserUtility::$databaseUsers[$userId];
        }
        $result = DatabaseUtility::getConnection()->query("SELECT * FROM users WHERE user_id = '".$userId."'");
        if ($result) {
            UserUtility::$databaseUsers[$userId] = $result->fetch_assoc();
            return UserUtility::$databaseUsers[$userId];
        } else {
            throw new \Exception('User "'.$userId.'" not found');
        }
    }

    public static function getAllUsers() {
        $result = DatabaseUtility::getConnection()->query("SELECT * FROM users");
        if ($result) {
            foreach($result->fetch_all(MYSQLI_ASSOC) as $user) {
                UserUtility::$databaseUsers[$user['user_id']] = $user;
            }

            return UserUtility::$databaseUsers;
        } else {
            throw new \Exception('Users could not be loaded');
        }
    }
}