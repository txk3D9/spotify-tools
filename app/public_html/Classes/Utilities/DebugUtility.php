<?php

namespace SpotifyTools\Utilities;

class DebugUtility
{

    public static function setXDebugSettings()
    {
		ini_set('xdebug.var_display_max_depth', '10');
		ini_set('xdebug.var_display_max_children', '256');
		ini_set('xdebug.var_display_max_data', '1024');
    }
}