-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: mysql
-- Generation Time: Dec 06, 2020 at 02:58 AM
-- Server version: 10.5.5-MariaDB-1:10.5.5+maria~focal
-- PHP Version: 7.4.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `spotify_tools`
--

-- --------------------------------------------------------

--
-- Table structure for table `apipi_import_playlists`
--

CREATE TABLE `apipi_import_playlists` (
  `playlist_id` varchar(32) NOT NULL,
  `user_id` varchar(255) NOT NULL,
  `import_playlist_id` varchar(32) NOT NULL,
  `monday` tinyint(1) NOT NULL DEFAULT 0,
  `tuesday` tinyint(1) NOT NULL DEFAULT 0,
  `wednesday` tinyint(1) NOT NULL DEFAULT 0,
  `thursday` tinyint(1) NOT NULL DEFAULT 0,
  `friday` tinyint(1) NOT NULL DEFAULT 0,
  `saturday` tinyint(1) NOT NULL DEFAULT 0,
  `sunday` tinyint(1) NOT NULL DEFAULT 0,
  `import_time` time NOT NULL,
  `last_import` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `apipi_playlists`
--

CREATE TABLE `apipi_playlists` (
  `playlist_id` varchar(32) NOT NULL,
  `user_id` varchar(255) NOT NULL,
  `delete_tracks_if_not_in_import_playlists` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Table structure for table `apipi_import_playlists`
--

CREATE TABLE `apb_playlists` (
  `playlist_id` varchar(32) NOT NULL,
  `user_id` varchar(255) NOT NULL,
  `monday` tinyint(1) NOT NULL DEFAULT 0,
  `tuesday` tinyint(1) NOT NULL DEFAULT 0,
  `wednesday` tinyint(1) NOT NULL DEFAULT 0,
  `thursday` tinyint(1) NOT NULL DEFAULT 0,
  `friday` tinyint(1) NOT NULL DEFAULT 0,
  `saturday` tinyint(1) NOT NULL DEFAULT 0,
  `sunday` tinyint(1) NOT NULL DEFAULT 0,
  `backup_time` time NOT NULL,
  `last_backup` timestamp NULL DEFAULT NULL,
  `backup_name_scheme` VARCHAR (255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `user_id` varchar(255) NOT NULL,
  `access_token` varchar(512) NOT NULL,
  `refresh_token` varchar(255) NOT NULL,
  `token_expiration` int(11) NOT NULL,
  `php_session_id` varchar(255) NOT NULL,
  `display_name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Table structure for table `logs`
--


CREATE TABLE `logs` (
    `uid` int(11) NOT NULL,
    `log_level` varchar(255) DEFAULT NULL,
    `message` varchar(255) DEFAULT NULL,
    `data` longtext DEFAULT NULL,
    `request_id` varchar(255) DEFAULT NULL,
    `user_id` varchar(255) DEFAULT NULL,
    `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `current_tracks_history` (
    `id` int(11) NOT NULL,
    `user_id` varchar(255) NOT NULL,
    `track_id` varchar(255) NOT NULL,
    `timestamp` bigint(20) NOT NULL,
    `progress_ms` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_general_ci;

CREATE TABLE `recent_tracks` (
                                 `id` int(11) NOT NULL,
                                 `user_id` varchar(255) NOT NULL,
                                 `played_at` datetime(3) NOT NULL,
                                 `track_id` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_general_ci;

CREATE TABLE `tops_history` (
                                `id` int(11) NOT NULL,
                                `user_id` varchar(255) NOT NULL,
                                `kind` varchar(10) NOT NULL,
                                `time_range` varchar(16) NOT NULL,
                                `rank` int(11) NOT NULL,
                                `entity_id` varchar(255) NOT NULL,
                                `created_at` date NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_general_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `apipi_import_playlists`
--
ALTER TABLE `apipi_import_playlists`
  ADD PRIMARY KEY (`playlist_id`,`user_id`,`import_playlist_id`);

--
-- Indexes for table `apipi_playlists`
--
ALTER TABLE `apipi_playlists`
  ADD PRIMARY KEY (`playlist_id`,`user_id`);

--
-- Indexes for table `apipi_import_playlists`
--
ALTER TABLE `apb_playlists`
  ADD PRIMARY KEY (`playlist_id`,`user_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`);

--
-- Indexes for table `logs`
--
ALTER TABLE `logs`
    ADD PRIMARY KEY (`uid`),
  ADD KEY `request_id` (`request_id`);

--
-- AUTO_INCREMENT for table `logs`
--
ALTER TABLE `logs`
    MODIFY `uid` int(11) NOT NULL AUTO_INCREMENT;

--
-- Indizes für die Tabelle `current_tracks_history`
--
ALTER TABLE `current_tracks_history`
    ADD PRIMARY KEY (`id`);

ALTER TABLE `current_tracks_history`
    MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;

--
-- Indizes für die Tabelle `recent_tracks`
--
ALTER TABLE `recent_tracks`
    ADD PRIMARY KEY (`id`);

ALTER TABLE `recent_tracks`
    MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;

--
-- Indizes für die Tabelle `tops_history`
--
ALTER TABLE `tops_history`
    ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT für exportierte Tabellen
--

--
-- AUTO_INCREMENT für Tabelle `tops_history`
--
ALTER TABLE `tops_history`
    MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;


CREATE TABLE `tracks` (
                          `id` varchar(255) NOT NULL,
                          `name` varchar(255) NOT NULL,
                          `artists` text NOT NULL,
                          `uri` varchar(255) NOT NULL,
                          `image` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_general_ci;


--
-- Indizes der exportierten Tabellen
--

--
-- Indizes für die Tabelle `tracks`
--
ALTER TABLE `tracks`
    ADD PRIMARY KEY (`id`);



CREATE TABLE `playlists` (
                             `id` int(11) NOT NULL,
                             `playlistId` varchar(255) NOT NULL,
                             `snapshotId` varchar(255) NOT NULL,
                             `data` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_general_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `playlists`
--
ALTER TABLE `playlists`
    ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `playlists`
--
ALTER TABLE `playlists`
    MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
