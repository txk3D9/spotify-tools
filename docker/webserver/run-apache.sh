#!/bin/bash

#
# https://github.com/docker-library/php/blob/bdfd0fc1d19102cd6f951614072a51eabf7191bf/5.6/apache/apache2-foreground
#

set -e

# Apache gets grumpy about PID files pre-existing
rm -f /var/run/apache2/apache2.pid

export APACHE_ARGUMENTS=-Ddockerdevelopment

exec apache2ctl -D FOREGROUND